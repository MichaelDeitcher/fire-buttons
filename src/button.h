#ifndef _BUTTON_
#define _BUTTON_

#include <Arduino.h>
#include "FastLED.h"

#define NUM_BUTTONS 15
#define NUM_LEDS_PER_BUTTON 9

class Button {
private:
    uint8_t _LEDindex;
    CRGB* _leds;
    uint8_t _buttonIndex;
    bool isPressed = false;
    bool isMobileOn = false;
    int8_t coolnessIndex = -1;
public:
    uint8_t virtualButtonState = 0;

public:
  Button();
  static void init();
  static void setupButton(uint8_t buttonIndex, uint8_t LEDindex, CRGB* leds);
  static int loopButtons();
  static void onButtonPress(uint8_t buttonIndex);
  static void onButtonRelease(uint8_t buttonIndex);
  static void setVirtualButton(uint8_t buttonIndex, uint8_t data);
private:
  void setup(uint8_t buttonIndex, uint8_t LEDindex, CRGB* leds);
  void loop();
  void onPress();
  void onRelease();
  void onMobileOn();
  void onMobileOff();
  void inoise8_fire();
  void Fire2012WithPalette();
};

#endif
