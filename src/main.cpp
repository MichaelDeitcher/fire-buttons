#include "FastLED.h"
// #include <AltSoftSerial.h>
#include <SoftwareSerial.h>
#include <Keypad.h>
#include "button.h"

#define SECTION_0

#ifdef SECTION_0
SoftwareSerial headSerial(50,46); // RX, TX
// AltSoftSerial headSerial;
#else
#define SECTION_1
#endif

// How many leds in your strip?
const int NUM_LEDS = NUM_LEDS_PER_BUTTON * NUM_BUTTONS;

// For led chips like Neopixels, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806 define both DATA_PIN and CLOCK_PIN
#define DATA_PIN 6
// #define CLOCK_PIN 13

// Define the array of leds
CRGB leds[NUM_LEDS];

const byte ROWS = 5;
const byte COLS = 3;
char keys[ROWS][COLS] = {
  {0,1,2},
  {5,4,3},
  {6,7,8},
  {11,10,9},
  {12,13,14}
};
byte rowPins[ROWS] = {A0, A1, A2, A3, A4}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {3, 4, 5}; //connect to the column pinouts of the keypad

Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

int getKeyPressed(){
  #ifdef DEBUG_SERIAL
    String msg;
  #endif

  int key = -1;
  // Fills kpd.key[ ] array with up-to 10 active keys.
  // Returns true if there are ANY active keys.
  if (kpd.getKeys())
  {
      for (int i=0; i<LIST_MAX; i++)   // Scan the whole key list.
      {
          if ( kpd.key[i].stateChanged )   // Only find keys that have changed state.
          {
              key = kpd.key[i].kchar;
              switch (kpd.key[i].kstate) {  // Report active key state : IDLE, PRESSED, HOLD, or RELEASED
              case PRESSED:
                  Button::onButtonPress(key);
                  break;
              case HOLD:
                  break;
              case RELEASED:
                  Button::onButtonRelease(key);
                  break;
              case IDLE:
                  break;
              }
          }
      }
  }
  return key;
}

void setup() {
      Serial.begin(9600);
#ifdef SECTION_0
      Serial1.begin(9600);
      Serial2.begin(9600);
      headSerial.begin(9600);
#endif
      for(int i = 0; i<NUM_BUTTONS; i++){
        Button::setupButton(i, i*NUM_LEDS_PER_BUTTON, leds);
      }
  	  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
      FastLED.show();
}

void loop() {
  // READ DATA FROM iOS
  #ifdef SECTION_0
    while (Serial2.available() == 2) {
      uint8_t index = Serial2.read();
      uint8_t value = Serial2.read();
      if(index < NUM_BUTTONS)
        Button::setVirtualButton(index,value);
      else {
        Serial1.write(index-NUM_BUTTONS);
        Serial1.write(value);
      }
    }
    while (Serial2.available()>0){
      Serial2.read();
    }
  #else
    bool read = false;
    while (Serial.available() == 2) {
      uint8_t index = Serial.read();
      uint8_t value = Serial.read();
      Button::setVirtualButton(index,value);
    }
    while (Serial.available()>0){
      Serial.read();
    }
  #endif

// HANDLE BUTTONS AND LED CHANGES
  getKeyPressed();
  int buttonStates = Button::loopButtons();
  FastLED.show();

  // SEND DATA TO iOS
  #ifdef SECTION_0
  // Proxy other section data to iOS
  uint8_t section1Byte0 = 0;
  uint8_t section1Byte1 = 0;
  while (Serial1.available() >= 2) {
    section1Byte0 = Serial1.read();
    section1Byte1 = Serial1.read();
  }
  // Send our section data to iOS
  headSerial.write(lowByte(buttonStates));
  headSerial.write(highByte(buttonStates));
  headSerial.write(lowByte(section1Byte0));
  headSerial.write(lowByte(section1Byte1));
  headSerial.write(lowByte(0));
  headSerial.write(lowByte(0));
  headSerial.write(lowByte(0));
  headSerial.write(lowByte(0));
  #else
    Serial.write(lowByte(buttonStates));
    Serial.write(highByte(buttonStates));
  #endif
}
