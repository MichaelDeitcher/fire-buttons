#include "button.h"

uint32_t xscale = 40;                                                 // How far apart they are
uint32_t yscale = 1;                                                  // How fast they move

CRGBPalette16 currentPalette(    CRGB::Black, CRGB::Black, CRGB::Black, CRGB::Maroon,
                                 CRGB::DarkRed, CRGB::Red, CRGB::Red, CRGB::Red,
                                 CRGB::DarkOrange,CRGB::Orange, CRGB::Orange, CRGB::Orange,
                                 CRGB::Yellow, CRGB::Yellow, CRGB::Gray, CRGB::Gray);

#define COOLNESS_COLOR_COUNT 3
#define COOLNESS_COLOR_BUTTON_MAX 6
CRGB coolColors[COOLNESS_COLOR_COUNT] = {CRGB::DarkBlue , CRGB::Purple, CRGB::Green};
int8_t lastCoolnessIndexUsed = -1;
int8_t coolnessIndexUsedCount = COOLNESS_COLOR_BUTTON_MAX;

Button buttons[NUM_BUTTONS];

Button::Button(){
};

void Button::setVirtualButton(uint8_t buttonIndex, uint8_t data) {
  buttons[buttonIndex].virtualButtonState = data;
}

void Button::setup(uint8_t buttonIndex, uint8_t LEDindex, CRGB* leds){
  _buttonIndex = buttonIndex;
  _LEDindex = LEDindex;
  _leds = leds;
  coolnessIndex = -1;
}

void Button::setupButton(uint8_t buttonIndex, uint8_t LEDindex, CRGB* leds){
  buttons[buttonIndex].setup(buttonIndex, LEDindex, leds);
}

void Button::onButtonPress(uint8_t buttonIndex){
  buttons[buttonIndex].onPress();
}
void Button::onButtonRelease(uint8_t buttonIndex){
  buttons[buttonIndex].onRelease();
}


void Button::onPress(){
  isPressed = true;
  if( coolnessIndexUsedCount == COOLNESS_COLOR_BUTTON_MAX ){
    coolnessIndexUsedCount = 0;
    if(lastCoolnessIndexUsed+1 == COOLNESS_COLOR_COUNT)
      lastCoolnessIndexUsed = 0;
    else
      lastCoolnessIndexUsed += 1;
  }
  coolnessIndexUsedCount += 1;
  coolnessIndex = lastCoolnessIndexUsed;
}

void Button::onRelease(){
  isPressed = false;
  coolnessIndex = -1;
}

int Button::loopButtons(){
  int buttonStates = 0;
  for(int i = 0; i<NUM_BUTTONS; i++){
    Button &button = buttons[i];
    button.loop();
    if(button.isPressed){
      buttonStates = buttonStates | 1 << i;
    }
  }
  return buttonStates;
}

void Button::loop(){
  if(virtualButtonState == 1) {
    for(uint8_t i=0; i<NUM_LEDS_PER_BUTTON; i++){
      _leds[_LEDindex+i] = CRGB::White;
    }
    return;
  }

  if(coolnessIndex != -1){
    for(uint8_t i=0; i<NUM_LEDS_PER_BUTTON; i++){
      _leds[_LEDindex+i] = coolColors[coolnessIndex];
    }
  } else {
      inoise8_fire();
  }
}

void Button::inoise8_fire() {
  for(uint8_t j = 0; j < NUM_LEDS_PER_BUTTON; j++) {
    uint8_t i = _LEDindex+j;
    uint8_t k = j+_LEDindex;
    uint8_t index = inoise8(k*xscale,millis()*yscale/NUM_LEDS_PER_BUTTON);                                    // X location is constant, but we move along the Y at the rate of millis()
    // _leds[i] = ColorFromPalette(currentPalette, min(k*(index)>>6, 255), k*255/NUM_LEDS_PER_BUTTON, LINEARBLEND);   // With that value, look up the 8 bit colour palette value and assign it to the current LED.
    // _leds[i] = ColorFromPalette(currentPalette, min(k*(index)>>6, 255), k*255/NUM_LEDS_PER_BUTTON, LINEARBLEND);
    _leds[i] = ColorFromPalette(currentPalette, index);
    // _leds[i] = CRGB::Red;
  }                                                                                                    // The higher the value of i => the higher up the palette index (see palette definition).
                                                                                                       // Also, the higher the value of i => the brighter the LED.
}
